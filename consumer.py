# -*- coding: utf-8 -*-

import numpy as np
import os
import time


mydir = "."
lockfile="lockfile.txt"
tasklist="tasklist.txt"

consumer_delay = 1

i=0
keepRunning = True
while keepRunning:
    
    if not lockfile in os.listdir(mydir):
        with open(lockfile, "w") as f:
            f.write("consumer")
    
        
        if tasklist in os.listdir(mydir):
          with open(tasklist, "r") as f:
              next_task = f.readline().strip()
              if len(next_task)>0:
                  
                  # graceful death if requested
                  if next_task == "Kill Yourself!":
                      print("I was asked to kill myself... ok")
                      keepRunning = False
                  
                  else:
                      print("working on task " + next_task)
                      # do something meaningful with your life
                  
                  # re-write remaining tasks
                  remaining_tasks = f.readlines()
                  with open(tasklist, "w") as f:
                      for line in remaining_tasks:
                          f.write(line)
              else:
                  print("nothing to do...")
                      
        os.remove(mydir+"/"+lockfile)

        i+=1
        
        time.sleep(consumer_delay)
    else:
        print("waiting for producer to finish...")
        time.sleep(1)
