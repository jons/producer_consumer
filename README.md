# producer_consumer

Producer/Consumer project template for python3 with LabVIEW template in mind.

producer.py -- "producer" program: writes tasks as single line strings into a textfile
consumer.py -- "consumer" program: reads first line from task list filled by producer and works with the given task string

The reading/writing to the tasklist is restricted to one program at a time using a lockfile.

Configure the following variables according to your use case:
mydir    = "/home/john/awesome_dir" -- directory where to put/look for tasklist and lockfile
lockfile = "lockfile.txt"           -- filename of file to use for locking access to tasklist
tasklist = "tasklist.txt"           -- filename of task list

If you write "Kill Yourself!" as a single line into the tasklist, the consumer program will not
start executing a new task, but finish the current one and then gracefully exit.

As an example, consecutive strings "iteration 0", "iteration 1", ... are written into the tasklist
and the work to be done by the consumer consists of printing the iteration number to the screen.


Possible improvements:
Currently, the tasklist is read completely and re-written without the first line in each iteration of the consumer loop.
Change this into only reading up to a newline character and re-writing using something more efficient...

For ideas on how to improve this or suggestions for additional features, contact jonathan.schilling@ipp.mpg.de

Changelog:
2018-08-30 v1.0 initial version
