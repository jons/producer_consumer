# -*- coding: utf-8 -*-

import numpy as np
import os
import time


mydir = "."
lockfile="lockfile.txt"
tasklist="tasklist.txt"

producer_delay = 0.5

i=0
while True:
    if not lockfile in os.listdir(mydir):
        with open(lockfile, "w") as f:
            f.write("producer")
            
            
        with open(tasklist, "a") as f:
            
            # write some meaningful tasks for the consumer here
            f.write("iteration "+np.str(i)+"\n")
            print("produce " + np.str(i))
            
            # you may also ask the consumer to kill itself
            if i>0 and i%10==0:
                f.write("Kill Yourself!\n")
                print("kill")

        os.remove(mydir+"/"+lockfile)

        i+=1
        
        time.sleep(producer_delay)
    else:
        print("waiting for consumer to finish...")
        time.sleep(1)