!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                     !
! consumer program for basic producer/consumer scheme !
!                                                     !
! Jonathan Schilling, jonathan.schilling@ipp.mpg.de   !
!                                                     !
! changelog:                                          !
!  2018-08-30 v1.0 creation                           !
!                                                     !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

program consumer
  implicit none
  
  logical :: keep_running,                            &
  &          lockfile_exists,                         &
  &          tasklist_exists
  integer :: iunit_lockfile,                          &
  &          iunit_tasklist,                          &
  &          iunit_temp_tasklist,                     &
  &          ioerr
  character(len=*), parameter ::                      &
  &          mydir    = "tasks",                      &
  &          lockfile = "lockfile.txt",               &
  &          tasklist = "tasklist.txt"
  character(len=50) :: current_task
  
  ! unit number for lockfile, hopefully free
  ! TODO: use safe_open_mod
  iunit_lockfile = 20
  
  ! unit number for tasklist, hopefully free
  ! TODO: use safe_open_mod
  iunit_tasklist = 21
  
  ! unit number for temporary tasklist, hopefully free
  ! TODO: use safe_open_mod
  iunit_temp_tasklist = 22
  
  ! initially, we want this consumer to keep running
  keep_running = .TRUE.
  
  ! keep running while not killed
  do while (keep_running)
  
    ! check whether lockfile exists in mydir
    inquire(file=mydir // "/" // lockfile, exist=lockfile_exists)
    
    ! if it does not exist, we are safe to check for new tasks
    if (.not. lockfile_exists) then
      
      ! create lockfile and write consumer name into it
      open(unit=iunit_lockfile, &
      &    file=mydir // "/" // lockfile, &
      &    status='new', &
      &    action='write', &
      &    iostat=ioerr)
      
      ! check whether lockfile could be opened correctly
      if (ioerr == 0) then
        
        ! lockfile was created, so write to it the current consumer name and close it again
        write(iunit_lockfile, *) "consumer"
        close(iunit_lockfile)
        
        ! check whether tasklist exists
        inquire(file=mydir // "/" // tasklist, exist=tasklist_exists)
        
        current_task = ""
        
        ! if the tasklist exists, open it and check what there is to do
        if (tasklist_exists) then
          
          ! open tasklist and read all tasks
          open(unit=iunit_tasklist, &
          &    file=mydir // "/" // tasklist, &
          &    status='old', &
          &    action='read', &
          &    iostat=ioerr)
          
          ! check whether opening of tasklist was successful
          if (ioerr == 0) then
            
            ! read current task
            read (iunit_tasklist, '(A)') current_task
            close(iunit_tasklist)
            if (len(current_task)>0) then
              write (6,*) current_task
            end if
            
            ! delete current task from tasklist (1...4)
            ! 1) create new temporary task list "temp_tasklist.txt"
            open(unit=iunit_temp_tasklist, &
            &    file=mydir // "/temp_tasklist.txt", &
            &    status='new', &
            &    action='write', &
            &    iostat=ioerr)
            
            ! 2) write remaining tasks to it
            if (ioerr == 0) then
              
              ! 3) delete previous tasklist
              ! 4) rename temporary tasklist to original tasklist
            
            
            
            
            else:
              
            
            
            end if
            
            
            
            
            
          
          else
            ! error opening tasklist
            write(*,*) "Error opening tasklist: " // mydir // "/" // tasklist
            keep_running = .FALSE.
          end if
          
          
          
          
        end if
        
        ! remove lockfile
        open(unit=iunit_lockfile, &
        &    file=mydir // "/" // lockfile, &
        &    status='old', &
        &    iostat=ioerr)
        if (ioerr == 0) close(iunit_lockfile, status='delete')
        
        
        
        
        ! actually execute current_task
        
        
        
        
        
        
        
        ! small delay before next check => minimize CPU and disk load
        
        
        
        
      else
        ! error opening lockfile
        write(*,*) "Error opening lockfile: " // mydir // "/" // lockfile
        keep_running = .FALSE.
      end if
    end if
    
  end do
  
  
  
  
end program consumer
